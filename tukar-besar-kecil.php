<?php
function tukar_besar_kecil($string){
	$reformed=[];
	$splitted = str_split($string);
	$j=count($splitted);
	for ($i=0; $i < $j ; $i++) { 
		if ($splitted[$i] == strtolower($splitted[$i])) {
			$reformed[] = strtoupper($splitted[$i]);
		} else{
			$reformed[] = strtolower($splitted[$i]);
		}
	}
	$final=implode($reformed);
	return $final;
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "<br>";
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "<br>";
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "<br>";
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "<br>";
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>