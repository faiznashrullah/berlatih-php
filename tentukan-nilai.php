<?php
function tentukan_nilai($number)
{
    if ($number <= 100 && $number >= 85) {
    	$result = "Sangat Baik <br>";
    } elseif ($number < 85 && $number >= 70) {
    	$result = "Baik <br>";
    } elseif ($number < 70 && $number >= 60) {
    	$result = "Cukup <br>";
    } else {
    	$result = "Kurang <br>";
    }
    return $result;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>