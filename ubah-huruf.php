<?php
function ubah_huruf($string){
	
	$translated=[];
	$splitted = str_split($string);
	$j=count($splitted);
	for ($i=0; $i < $j ; $i++) { 
		$temp = ord($splitted[$i]) + 1;
		$translated[] = chr($temp);
	}
	$final=implode($translated);
	return $final;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>